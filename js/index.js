document.addEventListener("DOMContentLoaded", function () {
    const imageButtons = document.querySelectorAll(".image-button");
    const overlay = document.querySelector(".overlay");
    const overlayImage = document.getElementById("overlay-image");
    const closeOverlay = document.getElementById("close-overlay");
    const prevButton = document.getElementById("prev-button");
    const nextButton = document.getElementById("next-button");
    const box = document.querySelector(".box");
    let currentImageIndex = 0;

    function showImage(imageName) {
        overlayImage.src = "img/" + imageName;
        overlay.classList.add("active");
    }

    function showNextImage() {
        currentImageIndex = (currentImageIndex + 1) % imageButtons.length;
        const imageName = imageButtons[currentImageIndex].getAttribute("data-img");
        showImage(imageName);
    }

    function showPrevImage() {
        currentImageIndex = (currentImageIndex - 1 + imageButtons.length) % imageButtons.length;
        const imageName = imageButtons[currentImageIndex].getAttribute("data-img");
        showImage(imageName);
    }

    imageButtons.forEach(function (button, index) {
        button.addEventListener("click", function () {
            currentImageIndex = index;
            const imageName = button.getAttribute("data-img");
            showImage(imageName);
        });
    });

    box.addEventListener("click", function (event) {
        if (event.target.tagName === "SPAN") {
            const dataSrc = event.target.getAttribute("data-src");
            if (dataSrc) {
                overlayImage.src = dataSrc;
                overlay.classList.add("active");
            }
        }
    });

    prevButton.addEventListener("click", showPrevImage);
    nextButton.addEventListener("click", showNextImage);

    closeOverlay.addEventListener("click", function () {
        overlay.classList.remove("active");
    });

     // Controlador de eventos para la tecla "Arrow Right" (derecha)
     document.addEventListener("keydown", function (event) {
        if (event.key === "ArrowRight" && overlay.classList.contains("active")) {
            showNextImage();
        }
    });

    // Controlador de eventos para la tecla "Arrow Left" (izquierda)
    document.addEventListener("keydown", function (event) {
        if (event.key === "ArrowLeft" && overlay.classList.contains("active")) {
            showPrevImage();
        }
    });
});


